
************
Introduction
************

.. figure:: /images/sculpt-paint_brush_introduction_brush-circle.png
   :align: center

   Brush cursor.

The brush is the main way of interacting with any painting and sculpting mode.
While click & dragging in the 3D Viewport it will create a :doc:`stroke </sculpt_paint/brush/stroke>`
and apply an effect depending on various brush settings.

.. tip::

   It is highly recommended to use a :ref:`Graphics Tablet <hardware-tablet>`
   for a better brush feel and additional features.


Brush Control
=============

These are the most common hotkeys for controlling the brush.

- Set brush size :kbd:`F`
- Set brush strength :kbd:`Shift-F`
- Rotate brush texture / Set brush weight :kbd:`Ctrl-F`

After pressing these hotkeys, you can then either adjust the value interactively or by typing in numbers.
Move the mouse right or left to increase/reduce the value
(additionally with precision (:kbd:`Shift`) and/or snapping (:kbd:`Ctrl`) activated).
Finally confirm (:kbd:`LMB`, :kbd:`Return`) or cancel (:kbd:`RMB`, :kbd:`Esc`).

You can also invert the brush direction/effect by holding :kbd:`Ctrl`.
